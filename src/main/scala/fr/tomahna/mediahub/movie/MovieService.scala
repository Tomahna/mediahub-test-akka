package fr.tomahna.mediahub.movie

import java.nio.file.Path

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.alpakka.csv.scaladsl.CsvToMap
import akka.stream.scaladsl._
import akka.util.ByteString
import cats.instances.map.catsKernelStdCommutativeMonoidForMap
import cats.instances.int.catsKernelStdGroupForInt
import cats.instances.future.catsStdInstancesForFuture
import cats.instances.list.catsStdInstancesForList
import cats.instances.set.catsStdShowForSet
import cats.instances.string.catsKernelStdOrderForString
import cats.kernel.Eq
import cats.Show
import cats.syntax.all._
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder
import fr.tomahna.mediahub.movie.MovieService._
import fr.tomahna.mediahub.movie.TsvDecoder._

import scala.concurrent.ExecutionContext
import akka.stream.SourceShape
import scala.concurrent.Future
import scala.util.Try

object MovieService {
  final case class TitleId(value: ByteString) extends AnyVal
  object TitleId {
    implicit val eq: Eq[TitleId] = Eq.fromUniversalEquals
    implicit val show: Show[TitleId] = _.value.utf8String
  }
  final case class NameId(value: ByteString) extends AnyVal
  object NameId {
    implicit val show: Show[NameId] = _.value.utf8String
  }

  final case class TitleBasics(
      tconst: TitleId,
      primaryTitle: ByteString,
      originalTitle: ByteString,
      startYear: ByteString,
      endYear: ByteString,
      genres: ByteString
  )
  object TitleBasics {
    implicit val tsvDecoder: TsvDecoder[TitleBasics] = line =>
      TitleBasics(
        TitleId(line(0)),
        line(2),
        line(3),
        line(5),
        line(6),
        Try(line(8)).getOrElse(ByteString.empty)
      )
  }

  final case class TitleEpisode(parentTconst: TitleId)
  object TitleEpisode {
    implicit val tsvDecoder: TsvDecoder[TitleEpisode] = line =>
      TitleEpisode(TitleId(line(1)))
  }

  final case class TitlePrincipals(tconst: TitleId, nconst: NameId)
  object TitlePrincipals {
    implicit val tsvDecoder: TsvDecoder[TitlePrincipals] =
      line => TitlePrincipals(TitleId(line(0)), NameId(line(2)))
  }

  final case class NameBasics(
      nconst: NameId,
      primaryName: ByteString,
      birthYear: ByteString,
      deathYear: ByteString,
      primaryProfession: ByteString
  )
  object NameBasics {
    implicit val tsvDecoder: TsvDecoder[NameBasics] =
      line => NameBasics(NameId(line(0)), line(1), line(2), line(3), line(4))
  }

  final case class Principal(
      name: String,
      birthYear: Option[Int], //some principal do not have a birthyear (ex: nm0660141)
      deathYear: Option[Int],
      profession: List[String]
  )
  object Principal {
    implicit val encoder: Encoder[Principal] = deriveEncoder
  }

  final case class TvSeries(
      original: String,
      startYear: Int,
      endYear: Option[Int],
      genres: List[String]
  )
  object TvSeries {
    implicit val encoder: Encoder[TvSeries] = deriveEncoder
  }
}

trait MovieService {
  def principalsForMovieName(name: String): Source[Principal, _]
  def tvSeriesWithGreatestNumberOfEpisodes(): Source[TvSeries, _]
}

class MovieServiceImpl(
    nameBasics: Seq[Path],
    titleBasics: Seq[Path],
    titlePrincipals: Seq[Path],
    titleEpisode: Seq[Path]
)(implicit ec: ExecutionContext, mat: Materializer)
    extends MovieService {

  def principalsForMovieName(name: String): Source[Principal, _] = {
    val titlePrincipalsId = for {
      titleId <- findTitleId(ByteString.fromString(name)).runWith(Sink.head)
      _ = println(show"TitleId is $titleId")
      titlePrincipalsId <- findPrincipalsIds(titleId).runWith(
        Sink.collection[NameId, Set[NameId]]
      )
      _ = println(show"TitlePrincipalsId are $titlePrincipalsId")
    } yield titlePrincipalsId

    Source
      .fromFuture(titlePrincipalsId)
      .flatMapConcat { titlePrincipalsId =>
        parallely(nameBasics) {
          uncompressAndDecoderFlow[NameBasics].filter(
            principals => titlePrincipalsId.contains(principals.nconst)
          )
        }
      }
      .map(
        n =>
          Principal(
            n.primaryName.utf8String,
            Option(n.birthYear.utf8String).filter(_ =!= "\\N").map(_.toInt),
            Option(n.deathYear.utf8String).filter(_ =!= "\\N").map(_.toInt),
            n.primaryProfession.utf8String.split(',').toList
          )
      )
  }

  def tvSeriesWithGreatestNumberOfEpisodes(): Source[TvSeries, _] = {
    Source
      .fromFuture(episodeCount())
      .flatMapConcat { greatestNumberOfEpisodesIds =>
        println(greatestNumberOfEpisodesIds.map(_.value.utf8String))
        parallely(titleBasics) {
          uncompressAndDecoderFlow[TitleBasics].filter(
            basics => greatestNumberOfEpisodesIds.contains(basics.tconst)
          )
        }
      }
      .map { b =>
        TvSeries(
          original = b.originalTitle.utf8String,
          startYear = b.startYear.utf8String.toInt,
          endYear =
            Option(b.endYear.utf8String).filter(_ =!= "\\N").map(_.toInt),
          genres = b.genres.utf8String.split('\n').toList
        )
      }
  }

  private def episodeCount(): Future[Set[TitleId]] = {
    val counts = for {
      path <- titleEpisode
    } yield FileIO
      .fromPath(path)
      .via(uncompressAndDecoderFlow[TitleEpisode])
      .runFold(Map.empty[TitleId, Int]) { (acc, episode) =>
        acc.get(episode.parentTconst) match {
          case Some(count) => acc + (episode.parentTconst -> (count + 1))
          case None        => acc + (episode.parentTconst -> 1)
        }
      }

    counts.toList
      .sequence[Future, Map[TitleId, Int]]
      .map(_.reduce(_ combine _))
      .map(
        _.toList
          .sortBy { case (title, count) => -count }
          .take(10)
          .map(_._1)
          .toSet
      )
  }

  private def findPrincipalsIds(tcons: TitleId): Source[NameId, _] =
    parallely(titlePrincipals) {
      uncompressAndDecoderFlow[TitlePrincipals]
        .filter(_.tconst === tcons)
        .map(_.nconst)
    }

  private def findTitleId(name: ByteString): Source[TitleId, _] =
    parallely(titleBasics) {
      uncompressAndDecoderFlow[TitleBasics]
        .filter(
          basic => basic.primaryTitle === name || basic.originalTitle === name
        )
        .map(_.tconst)
    }

  private def uncompressAndDecoderFlow[T: TsvDecoder]
      : Flow[ByteString, T, NotUsed] =
    Flow[ByteString]
      .via(CsvParsing.lineScanner(delimiter = '\t', escapeChar = '\r').async)
      .map(line => line.decode[T])

  implicit val ByteStringEq: Eq[ByteString] = Eq.fromUniversalEquals

  private def parallely[T](
      paths: Seq[Path]
  )(flow: Flow[ByteString, T, _]): Source[T, NotUsed] = Source.fromGraph {
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val merge = builder.add(Merge[T](paths.length))
      for {
        (path, index) <- paths.zipWithIndex
      } yield {
        val input = builder.add {
          FileIO
            .fromPath(path)
            .via(flow.async)
        }

        input ~> merge.inlets(index)
      }

      SourceShape(merge.out)
    }
  }
}
