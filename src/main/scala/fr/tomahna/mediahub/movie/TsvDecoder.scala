package fr.tomahna.mediahub.movie

import akka.util.ByteString

trait TsvDecoder[T] {
  def decode(line: List[ByteString]): T
}

object TsvDecoder {
  implicit class TsvDecodable(line: List[ByteString]) {
    def decode[T](implicit decoder: TsvDecoder[T]): T =
      decoder.decode(line)
  }
}
