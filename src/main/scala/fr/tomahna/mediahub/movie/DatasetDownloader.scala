package fr.tomahna.mediahub.movie

import java.nio.file.Path
import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.stream.IOResult
import akka.stream.Materializer
import akka.stream.SinkShape
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.scaladsl._
import akka.util.ByteString

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import akka.NotUsed
import akka.stream.ClosedShape

object DatasetDownloader {

  def downloadDataset(name: String, partitions: Int = 8)(
      implicit ec: ExecutionContext,
      as: ActorSystem,
      mat: Materializer
  ): Future[Seq[Path]] = {
    val paths = (0 until partitions)
      .map(n => name.replace(".gz", s".$n"))
      .map(file => Paths.get(s"/tmp/$file"))
    if (paths.exists(path => !path.toFile().exists())) {
      println(s"Downloading $name")
      for {
        response <- Http().singleRequest(
          HttpRequest(uri = s"https://datasets.imdbws.com/$name")
        )
        _ <- partitionCompressedTsv(response.entity.dataBytes, paths).run()
        _ = println(s"Downloaded $name")
      } yield paths
    } else Future.successful(paths)
  }

  private def partitionCompressedTsv(
      source: Source[ByteString, _],
      paths: Seq[Path]
  ) = RunnableGraph.fromGraph {
    GraphDSL.create(Sink.ignore) { implicit builder => sink =>
      import GraphDSL.Implicits._

      val tsvIn = builder.add {
        source
          .via(Compression.gunzip().async)
          .via(Framing.delimiter(ByteString('\n'), 10000000))
          .drop(1) //drop header
      }

      val broadcast = builder.add(Broadcast[ByteString](2))
      val dispatcher = builder.add(Balance[ByteString](paths.length))

      tsvIn ~> broadcast ~> dispatcher
      broadcast ~> sink
      for {
        (path, index) <- paths.zipWithIndex
      } yield {
        val lineDelimiter = ByteString('\n')
        val tsvOut = builder.add {
          Flow[ByteString]
            .map(_ concat lineDelimiter)
            .to(FileIO.toPath(path).async)
        }

        dispatcher.out(index) ~> tsvOut
      }

      ClosedShape
    }

  }
}
