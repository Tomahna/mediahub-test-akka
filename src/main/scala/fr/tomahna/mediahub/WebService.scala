package fr.tomahna.mediahub

import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.model.Uri.Path.Segment
import akka.stream.scaladsl.Sink
import akka.stream.Materializer
import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import fr.tomahna.mediahub.movie.MovieService
import fr.tomahna.mediahub.movie.MovieService.Principal

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

class WebService(movieService: MovieService)(
    implicit ec: ExecutionContext,
    mat: Materializer
) extends Directives {

  val route = {
    pathPrefix("title" / Segment) { name =>
      path("principal") {
        complete(movieService.principalsForMovieName(name))
      }
    } ~
      path("show" / "longest") {
        get {
          complete(movieService.tvSeriesWithGreatestNumberOfEpisodes())
        }
      }
  }
}
