package fr.tomahna.mediahub

import java.nio.file.Path
import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import cats.instances.future.catsStdInstancesForFuture
import cats.syntax.all._
import com.typesafe.config.ConfigFactory
import fr.tomahna.mediahub.movie.DatasetDownloader.downloadDataset
import fr.tomahna.mediahub.movie.MovieServiceImpl

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

object WebServer {
  def main(args: Array[String]) {
    implicit val system = ActorSystem("server-system")
    implicit val materializer = ActorMaterializer()

    val config = ConfigFactory.load()
    val interface = config.getString("http.interface")
    val port = config.getInt("http.port")

    val app = for {
      nameBasics <- downloadDataset("name.basics.tsv.gz")
      titleBasics <- downloadDataset("title.basics.tsv.gz")
      titlePrincipals <- downloadDataset("title.principals.tsv.gz")
      titleEpisodes <- downloadDataset("title.episode.tsv.gz")
    } yield {
      val movieService =
        new MovieServiceImpl(
          nameBasics,
          titleBasics,
          titlePrincipals,
          titleEpisodes
        )
      val service = new WebService(movieService)

      Http().bindAndHandle(service.route, interface, port)

      println(s"Server online at http://$interface:$port")
    }

    Await.result(app, 1.hour)
  }
}
