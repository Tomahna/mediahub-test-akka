scalaVersion := "2.13.1"
organization := "fr.tomahna"
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % "10.1.11",
  "com.typesafe.akka" %% "akka-stream" % "2.6.1",
  "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "1.1.2",
  "de.heikoseeberger" %% "akka-http-circe" % "1.30.0",
  "io.circe" %% "circe-core" % "0.12.3",
  "io.circe" %% "circe-generic" % "0.12.3",
  "io.circe" %% "circe-parser" % "0.12.3",
  "org.typelevel" %% "cats-core" % "2.0.0",
  "org.scalatest" %% "scalatest" % "3.1.0" % "test"
)
enablePlugins(JavaAppPackaging)
