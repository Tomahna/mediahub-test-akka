# IMDB Service

## Decisions de design

Le parti pris, ici, est de travailler avec des datasets relativement "raw". Sur une véritable application de ce type, il serait probablement souhaitable de séparer le pipeline d'ingestion de donnée du requêtage et d'utiliser une base de donnée intermédiaire.

## Utilisation

``` shell
sbt run
```

``` shell
curl http://localhost:8080/title/Baba%20Yaga/principal
```

``` shell
curl http://localhost:8080/show/longest
```

## Axes d'amélioration

Le parsing et la gestion d'erreurs sont très lacunaires.
